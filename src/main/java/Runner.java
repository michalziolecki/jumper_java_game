import javafx.application.Application;

import javafx.stage.Stage;

public class Runner extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {

        WelcomeWindow welcomeWindow = new WelcomeWindow(primaryStage);
        welcomeWindow.setParametersOfWindow();
        welcomeWindow.setText();
        welcomeWindow.uploadGraphics();

        primaryStage.show();
    }
}
