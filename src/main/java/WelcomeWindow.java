import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class WelcomeWindow {

    private Stage stage;
    private Scene scene;
    private Group effectsContainer;
    private Canvas graphicsSurface;
    private GraphicsContext graphicsContainer;

    public WelcomeWindow(Stage primaryStage){
        this.stage = primaryStage;
        this.effectsContainer = new Group();
        this.graphicsSurface = new Canvas();
        this.graphicsContainer = graphicsSurface.getGraphicsContext2D();
        this.scene = new Scene(effectsContainer);
        this.effectsContainer.getChildren().add(graphicsSurface);
        this.stage.setScene(scene);
    }

    public void setParametersOfWindow(){
        stage.setTitle("Jumper");
        Rectangle2D sizeOfScreen = getScreenSize();
        graphicsSurface.setHeight(sizeOfScreen.getHeight() - 15);
        graphicsSurface.setWidth(sizeOfScreen.getWidth());
    }

    public Rectangle2D getScreenSize(){
        Rectangle2D sizeOfScreen = Screen.getPrimary().getVisualBounds();
        return sizeOfScreen;
    }

    public void setText(){
        graphicsContainer.setFill(Color.GOLD);
        graphicsContainer.setStroke(Color.BLACK);
        graphicsContainer.setLineWidth(1);
        Font theFont = Font.font( "Times New Roman", FontWeight.BOLD, 48 );
        graphicsContainer.setFont( theFont );
        graphicsContainer.fillText( "Welcome in Jumper!", graphicsSurface.getHeight()/2, 100 );
        graphicsContainer.strokeText( "Welcome in Jumper!", graphicsSurface.getHeight()/2, 100 );
    }

    public void uploadGraphics(){
        Image characterImage = new Image("file:C:\\Programming\\gameJava\\" +
                "jumper\\JumperGraphics\\character\\PNG\\Player\\Poses\\player_action1.png");
        graphicsContainer.drawImage(characterImage,  characterImage.getHeight(), characterImage.getWidth());
    }

}
